from fabric import Connection

class Apache:

    def __init__(self, host: str, user: str, password: str) -> None:
        self.connection = Connection(host = host, user = user, connect_kwargs = {'password': password})
    
    def is_installed(self) -> bool:
        command = "sudo systemctl status apache2 | grep -m 1 pache2"
        result = self.connection.run(command, pty = True).stdout
        if result:
            return True
        
        return False
        
    def is_running(self) -> bool:
        return self.connection.run('sudo systemctl status apache2 | grep running > /dev/null', warn = True, pty = True).ok
    
    def log(self, file: str = 'error') -> str:
        return self.connection.run(f'cat /var/log/apache2/{file}.log', hide = True)
    
    def update_files(self, local_path: str, remote_path: str) -> str:
        try:
            with self.connection as conn:
                conn.put(local = local_path, remote = remote_path)
                return f'Fichiers mis à jour avec succès: {local_path} -> {remote_path}'
        except Exception as e:
            return f'Une erreur est survenu lors de la mise a jour des fichiers: {e}'
